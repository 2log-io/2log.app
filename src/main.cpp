#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "Init2logQMLComponents.h"
#include "Init2logQMLControls.h"
#include "CppHelper.h"
#include "InitQuickHub.h"
#include <QLocale>
#include <QTranslator>
#include <QQmlContext>


#if defined(Q_OS_ANDROID)
#include <AndroidHelper.h>
#endif

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
//    QGuiApplication::setHighDpiScaleFactorRoundingPolicy(Qt::HighDpiScaleFactorRoundingPolicy::Round);
    QCoreApplication::setOrganizationName("2log");
    QCoreApplication::setOrganizationDomain("2log.io");
    QCoreApplication::setApplicationName("2log App");

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    InitQuickHub::registerTypes("CloudAccess");
    Init2logQMLComponents::registerTypes("AppComponents", &engine);
    Init2logQMLControls::registerTypes("UIControls", &engine);
    CppHelper* keyHandler = new CppHelper(qApp);
    qApp->installEventFilter(keyHandler);
    engine.rootContext()->setContextProperty("cppHelper",keyHandler);

#if defined(Q_OS_ANDROID)
    AndroidHelper* androidHelper = new AndroidHelper(&app);
    androidHelper->setMenuStyle();
    engine.rootContext()->setContextProperty("mobileHelper", androidHelper);
#endif
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
