QT += quick
QT += network

CONFIG += c++17
CONFIG += sdk_no_version_check

include(quickhub-qmlclientmodule/QHClientModule.pri)
include(qml/2log-qmlcomponents/src/2log-qmlcomponents.pri)
include(qml/2log-qmlcontrols/src/2log-qmlcontrols.pri)
include(qrutils/SCodes/src/SCodes.pri)

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp

android {

SOURCES += \
        AndroidHelper.cpp

HEADERS += \
    AndroidHelper.h

}

RESOURCES += qml/qml.qrc

CONFIG += lrelease
#CONFIG += embed_translations

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle.properties \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml

ios {
    HEADERS += ios/src/Notch.h
    OBJECTIVE_SOURCES += ios/src/Notch.mm


    QMAKE_ASSET_CATALOGS += $$PWD/ios/assets/Images.xcassets
    QMAKE_TARGET_BUNDLE_PREFIX = io.2log
    include(infoplist.pri)
}

android: include(android/android_openssl/openssl.pri)

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android


