#ifndef ANDROIDHELPER_H
#define ANDROIDHELPER_H

#include <QtAndroid>
#include <QAndroidIntent>
#include <QObject>

class AndroidHelper : public QObject
{
    Q_OBJECT

public:
    explicit AndroidHelper(QObject *parent = nullptr);
    Q_INVOKABLE QString getIntentString();
    void setMenuStyle();

signals:

};

#endif // ANDROIDHELPER_H
