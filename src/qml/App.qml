import QtQuick 2.0
import UIControls 1.0
import CloudAccess 1.0
import AppComponents 1.0
import QtQuick.Layouts 1.0

Item {
    id: app
    anchors.fill: parent
    property alias userModel: userObjModel
    Rectangle {
        anchors.fill: parent
        color: Colors.darkBlue
        Rectangle {
            anchors.fill: parent
            color: "black"
            opacity: .15
        }
    }

    Stack {
        id: stackView
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: menu.top
        initialItem: qrScan
    }

    BottomAppMenu {
        id: menu
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left

        open: true

        stackView: stackView

        BottomAppMenuButton {
            text: qsTr("Code Scannen")
            icon: Icons.qrCode

            selected: stackView.depth > 0 && stackView.currentItem
                      !== null ? stackView.currentItem.viewID === "qr" : false
            onClicked: {
                stackView.replace(null, qrScan)
            }
        }

        BottomAppMenuButton {
            text: qsTr("Kontoübersicht")
            icon: Icons.user

            selected: stackView.depth > 0 && stackView.currentItem
                      !== null ? stackView.currentItem.viewID === "overview" : false
            onClicked: {
                stackView.replace(null, overview)
            }
        }

        BottomAppMenuButton {
            text: qsTr("Logout")
            icon: Icons.logout

            onClicked: {
                UserLogin.autoLogIn = false
                UserLogin.logout()
                Connection.disconnectServer()
                root.loggedOut = true
            }
        }
    }
    FilteredDeviceModel {
        id: deviceModel
        deviceType: ["Controller/AccessControl", "Controller"]
    }

    Component {
        id: qrScan
        ScanCode {}
    }

    Component {
        id: overview
        Overview {}
    }

    SynchronizedObjectModel {
        resource: "labcontrol/user"
        id: userObjModel
    }
}
