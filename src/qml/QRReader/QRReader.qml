import QtQuick 2.12
import QtQuick.Controls 2.12
import QtMultimedia 5.12
import com.scythestudio.scodes 1.0
import CloudAccess 1.0
import AppComponents 1.0
import UIControls 1.0

Container {
    id: container
    doLayout: true
    headline: qsTr("Code scannen")
    managedContent: Item {
        //        Rectangle {
        //            anchors.fill: parent
        //            color: "red"
        //        }
        anchors.fill: parent
        visible: true

        //    width: Qt.platform.os == "android"
        //           || Qt.platform.os == "ios" ? Screen.width : camera.viewfinder.resolution.width
        //    height: Qt.platform.os == "android"
        //            || Qt.platform.os == "ios" ? Screen.height : camera.viewfinder.resolution.height
        Camera {
            id: camera
            focus {
                focusMode: CameraFocus.FocusContinuous
                focusPointMode: CameraFocus.FocusPointAuto
            }
        }

        VideoOutput {
            id: videoOutput
            source: camera
            anchors.fill: parent
            autoOrientation: true
            fillMode: VideoOutput.PreserveAspectCrop
            // add barcodeFilter to videoOutput's filters to enable catching barcodes
            filters: [barcodeFilter]

            onSourceRectChanged: {
                barcodeFilter.captureRect = videoOutput.mapRectToSource(
                            videoOutput.mapNormalizedRectToItem(Qt.rect(
                                                                    0.25, 0.25,
                                                                    0.5, 0.5)))
            }

            ScannerOverlay {
                id: scannerOverlay
                anchors.fill: parent

                captureRect: videoOutput.mapRectToItem(
                                 barcodeFilter.captureRect)
            }

            // used to get camera focus on touched point
            MouseArea {
                anchors.fill: parent
                onClicked: {

                    camera.focus.customFocusPoint = Qt.point(mouse.x / width,
                                                             mouse.y / height)
                    camera.focus.focusMode = CameraFocus.FocusMacro
                    camera.focus.focusPointMode = CameraFocus.FocusPointCustom
                }
            }
        }

        SBarcodeFilter {
            id: barcodeFilter

            // you can adjust capture rect (scan area) ne changing these Qt.rect() parameters
            captureRect: videoOutput.mapRectToSource(
                             videoOutput.mapNormalizedRectToItem(
                                 Qt.rect(0.25, 0.25, 0.5, 0.5)))

            onCapturedChanged: {
                active = false
                console.log("captured: " + captured)
                authService.call("authenticate", {
                                     "code": captured
                                 }, callBack)
            }
            function callBack(data) {

                if (data.errcode < 0) {
                    active = true
                } else {
                    successOverlay.visible = true
                }
            }
        }

        ServiceModel {
            id: authService
            service: "codeAuthenticator"
        }
        Rectangle {
            id: successOverlay
            color: Colors.backgroundDarkBlue
            anchors.fill: parent
            visible: false

            Column {
                anchors.centerIn: parent
                spacing: 20

                Rectangle {
                    width: 60
                    height: 60
                    radius: 30
                    color: Colors.okayGreen
                    anchors.horizontalCenter: parent.horizontalCenter
                    Icon {
                        iconSize: 32
                        icon: Icons.check
                        iconColor: Colors.white
                        anchors.centerIn: parent
                    }
                }
                TextLabel {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Das hat geklappt!")
                }

                StandardButton {
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: qsTr("Nochmal scannen")

                    onClicked: {
                        barcodeFilter.active = true
                        successOverlay.visible = false
                    }
                }
            }
        }
    }
}
