import QtQuick 2.12
import QtGraphicalEffects 1.12
import UIControls 1.0


/*!
  Area for scanning barcodes
  */
Item {
    id: root

    property rect captureRect

    Item {
        id: captureZoneCorners

        x: root.captureRect.x
        y: root.captureRect.y

        width: root.captureRect.width
        height: root.captureRect.height

        Rectangle {
            id: topLeftCornerH

            anchors {
                top: parent.top
                left: parent.left
            }

            width: 20
            height: 5

            color: Colors.white
            radius: height / 2
        }

        Rectangle {
            id: topLeftCornerV
            anchors {
                top: parent.top
                left: parent.left
            }

            width: 5
            height: 20

            color: Colors.white
            radius: width / 2
        }

        // ----------------------
        Rectangle {
            id: bottomLeftCornerH

            anchors {
                bottom: parent.bottom
                left: parent.left
            }

            width: 20
            height: 5

            color: Colors.white
            radius: height / 2
        }

        Rectangle {
            id: bottomLeftCornerV

            anchors {
                bottom: parent.bottom
                left: parent.left
            }

            width: 5
            height: 20

            color: Colors.white
            radius: width / 2
        }

        // ----------------------
        Rectangle {
            id: topRightCornerH

            anchors {
                top: parent.top
                right: parent.right
            }

            width: 20
            height: 5

            color: Colors.white
            radius: height / 2
        }

        Rectangle {
            id: topRightCornerV

            anchors {
                top: parent.top
                right: parent.right
            }

            width: 5
            height: 20

            color: Colors.white
            radius: width / 2
        }

        // ----------------------
        Rectangle {
            id: bottomRightCornerH

            anchors {
                bottom: parent.bottom
                right: parent.right
            }

            width: 20
            height: 5

            color: Colors.white
            radius: height / 2
        }

        Rectangle {
            id: bottomRightCornerV

            anchors {
                bottom: parent.bottom
                right: parent.right
            }

            width: 5
            height: 20

            color: Colors.white
            radius: width / 2
        }

        Rectangle {
            id: scanIndicator

            width: parent.width
            height: 1

            anchors {
                horizontalCenter: parent.horizontalCenter
            }

            color: Colors.highlightBlue

            SequentialAnimation {
                id: scanIndicatorAnimation

                loops: Animation.Infinite

                PropertyAnimation {
                    id: toTopAnimation
                    target: scanIndicator
                    property: "y"
                    duration: 2000
                }

                PropertyAnimation {
                    id: toBottomAnimation
                    target: scanIndicator
                    property: "y"
                    duration: 2000
                }
            }
        }

        RectangularGlow {
            id: effect

            width: scanIndicator.width / 2
            height: scanIndicator.height

            anchors.centerIn: scanIndicator

            glowRadius: 50
            spread: 0.1
            opacity: .5
            color: Colors.highlightBlue
            cornerRadius: glowRadius
        }
    }

    Item {
        id: mask
        visible: false
        anchors.fill: parent

        Rectangle {
            anchors.fill: parent
            opacity: .4
        }
        Rectangle {
            anchors.centerIn: parent
            radius: 10
            width: captureZoneCorners.width + 20
            height: captureZoneCorners.height + 20
        }
    }

    Rectangle {
        visible: false
        id: source
        anchors.fill: parent
        color: "black"
    }

    OpacityMask {
        anchors.fill: parent
        source: source
        maskSource: mask
        invert: true
    }

    onCaptureRectChanged: {
        toTopAnimation.to = 5
        toBottomAnimation.to = captureRect.height - 5
        scanIndicatorAnimation.start()
    }
}
