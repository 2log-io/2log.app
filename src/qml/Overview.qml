import QtQuick 2.0
import QtQuick.Controls 2.15
import UIControls 1.0
import CloudAccess 1.0
import QtQuick.Layouts 1.0
import AppComponents 1.0
import "QRReader"

ScrollViewBase {
    viewID: "overview"
    headline: qsTr("Übersicht")
    spacing: 10
    id: docroot

    Column {
        spacing: 30
        width: parent.width

        LastUserJobsContainer {
            height: 280
            visible: count != 0
            width: parent.width
        }


        Container
        {

            width: parent.width
            headline: qsTr("Passwort zurücksetzen")
            signal passwordChanged(bool success,string code)

            ColumnLayout
            {
                width: parent.width

                spacing: 20

                TextField
                {
                    id: originPass
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignVCenter
                    placeholderText:qsTr("Altes Passwort")
                    nextOnTab: pass1.field
                    field.echoMode: TextInput.Password
                }


                TextField
                {
                    id: pass1
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignVCenter
                    placeholderText: qsTr("Neues Passwort")
                    nextOnTab: pass2.field
                    field.echoMode: TextInput.Password
                }

                TextField
                {
                    id: pass2
                    Layout.fillWidth: true
                    Layout.alignment: Qt.AlignVCenter
                    placeholderText: qsTr("Neues Passwort wiederholen")
                    field.echoMode: TextInput.Password
                }


                StandardButton
                {

                    enabled: (pass1.text === pass2.text) && (originPass.text !== "") && (pass1.text !== "")
                    text:qsTr("Passwort ändern")
                    onClicked: UserLogin.changePassword(originPass.text, pass2.text, parent.changePassCb)
                }

                function changePassCb(success, code)
                {
                    if (success) {
                        feedbackOKDialog.open()
                    } else {
                        feedbackErrorDialog.open()
                    }
                    originPass.text = ""
                    pass1.text = ""
                    pass2.text = ""
                }
            }
        }

    }

    Item {
        InfoDialog {
            id: feedbackOKDialog
            icon: Icons.warning2
            anchors.centerIn: Overlay.overlay
            iconColor: Colors.highlightBlue
            text: qsTr("Passwort erfolgreich geändert!")
            StandardButton {
                text: "OK"
                onClicked: feedbackOKDialog.close()
            }
        }}
}
