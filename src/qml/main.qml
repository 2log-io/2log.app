

/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.4
import QtQuick 2.8
import UIControls 1.0
import CloudAccess 1.0
import AppComponents 1.0
import QtQuick.Window 2.12

ApplicationWindow {
    id: root
    width: 480
    height: 800
    visible: true
    title: qsTr("Hello World")
    color: "transparent"

    property int appState: Qt.application.state
    property string serverURL: ""
    property bool suspended
    property bool reconnect: false
    property bool isMobile: true
    property bool loggedOut: true

    Connections {
        target: Connection
        function onStateChanged() {
            if (Connection.state == Connection.STATE_Authenticated) {
                loggedOut = false
            }
        }
    }

    function suspend() {
        if (Connection.state == Connection.STATE_Authenticated) {
            root.suspended = true
            Connection.disconnectServer()
            root.reconnect = true
            root.loggedOut = true
        }
    }

    function activate() {
        if (root.reconnect) {
            root.reconnect = false
            Connection.reconnectServer()
        }
    }

    Connections {
        target: cppHelper
        function onBack() {
            root.close()
        }
    }

    onAppStateChanged: {
        if (isMobile) {
            switch (appState) {
            case Qt.ApplicationSuspended:
                suspend()
                break
            case Qt.ApplicationActive:
                activate()
                break
            }
        }
    }

    Timer {
        running: Connection.state == Connection.STATE_Disconnected
                 && !root.loggedOut
        interval: 5000
        onTriggered: Connection.reconnectServer()
    }

    Rectangle {
        anchors.fill: parent
        color: Colors.darkBlue
        Rectangle {
            anchors.fill: parent
            color: "black"
            opacity: .15
        }
    }

    LoginContainer {
        id: container
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        anchors.topMargin: 60
        enableServerTextField: !suspended || loggedOut
        states: [
            State {
                when: root.width < root.height
                AnchorChanges {
                    target: container
                    anchors.verticalCenter: undefined
                    anchors.top: container.parent.top
                    anchors.horizontalCenter: container.parent.horizontalCenter
                }
            }
        ]
    }

    App {
        visible: Connection.state == Connection.STATE_Authenticated
        anchors.fill: parent
    }
}
