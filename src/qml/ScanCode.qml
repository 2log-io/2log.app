import QtQuick 2.0
import UIControls 1.0
import CloudAccess 1.0
import QtQuick.Layouts 1.0
import AppComponents 1.0
import "QRReader"

ViewBase {
    viewID: "qr"
    headline: "Willkommen zurück, " + app.userModel.alias
    Component.onCompleted: console.log(JSON.stringify(UserLogin.currentUser))

    ColumnLayout {
        anchors.fill: parent
        QRReader {
            Layout.fillHeight: true
            Layout.fillWidth: true
        }

        ShowBalanceContainer {
            Layout.fillWidth: true
            Layout.minimumHeight: totalHeight
            Layout.maximumHeight: totalHeight
            headline: qsTr("Dein Kontostand")
            width: parent.width
            contentHeight: 100
            userModel: app.userModel
        }
    }
}
